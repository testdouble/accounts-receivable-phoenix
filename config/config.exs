# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :accounts_receivable_phoenix,
  ecto_repos: [AccountsReceivablePhoenix.Repo]

# Configures the endpoint
config :accounts_receivable_phoenix, AccountsReceivablePhoenixWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "9VvYKJU9sZQt8cXAO5vYujtBqZWV1f8W5H7rHoKAk94bI6lNtmrlokUTv2lkndO0",
  render_errors: [view: AccountsReceivablePhoenixWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: AccountsReceivablePhoenix.PubSub

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.12.18",
  default: [
    args: ~w(js/app.js --bundle --target=es2016 --outdir=../priv/static/assets),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
