# Accounts Receivable

Welcome to the Accounts Receivable system! This system is responsible for
creating, sending, and tracking invoices sent to clients so we can be paid for
our products and services.

## Getting started

### Prerequisites

Before you get started, be sure to:

* Be running a supported version of Elixir (we'd recommend version 1.12 or higher)
* That you have a recent version of git installed (e.g. `brew install git` on a Mac)

### Initial setup

Run a postgres instance with username: postgres, and password: postgres

Here's a quick example using docker (note make sure you stop any instance of postgres currently using port 5432).
```
docker run \
--name accounts-receivable-postgres \
-p 5432:5432 -e POSTGRES_PASSWORD=postgres -d \
postgres
```

This is a conventional Phoenix, so you should be able to get up and
running with the following commands:

```
$ mix deps.get
$ mix ecto.setup
```

No need to npm install assets since Phoenix 1.6 uses esbuild and compiling phoenix
takes care of frontend deps as well.

(Note that in addition to running migrations, we're also loading the app's
seeds into the development database to help move things along.)

If nothing blew up, you should be able to start the phoenix server:

```
$ mix phx.server
```

And then you should be able to see pre-generated invoices in a browser:
[http://localhost:4000](http://localhost:4000)
